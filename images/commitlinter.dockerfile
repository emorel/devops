FROM rust AS build
RUN rustup toolchain install stable
RUN rustup target add x86_64-unknown-linux-musl
RUN apt-get update && apt-get install -y musl-tools && rm -rf /var/lib/apt/lists/*
RUN cargo install conventional_commits_linter --target x86_64-unknown-linux-musl

FROM alpine:latest
RUN apk add git
COPY --from=build /usr/local/cargo/bin/conventional_commits_linter /bin/
